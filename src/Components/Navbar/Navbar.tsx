import React from "react";
import s from "./navBar.module.css";
import { NavLink } from "react-router-dom";

const Navbar: React.FC = () => {
  return (
    <div className={s.navBarWrapper}>
      <div>
        <NavLink
          to="/"
          className={({ isActive, isPending }) =>
            isPending ? "pending" : isActive ? "active" : ""
          }
        >
          Моя страница
        </NavLink>
      </div>
      <div>
        {" "}
        <NavLink
          to="/dialogs"
          className={({ isActive, isPending }) =>
            isPending ? "pending" : isActive ? "active" : ""
          }
        >
          Диалоги
        </NavLink>
      </div>
      <div>
        {" "}
        <NavLink
          to="/musiс"
          className={({ isActive, isPending }) =>
            isPending ? "pending" : isActive ? "active" : ""
          }
        >
          музыка
        </NavLink>
      </div>
      <div>
        {" "}
        <NavLink
          to="/news"
          className={({ isActive, isPending }) =>
            isPending ? "pending" : isActive ? "active" : ""
          }
        >
          Новости
        </NavLink>
      </div>
      <div>
        {" "}
        <NavLink
          to="/users"
          className={({ isActive, isPending }) =>
            isPending ? "pending" : isActive ? "active" : ""
          }
        >
          Пользователи
        </NavLink>
      </div>
    </div>
  );
};

export default Navbar;
