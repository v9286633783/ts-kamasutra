import React from "react";
import logo from "../../img/bear.jpg";
import s from "./header.module.css";
import { NavLink } from "react-router-dom";

const Header: React.FC = () => {
  return (
    <header className={s.header}>
      <img className={s.img} src={logo} alt="" />

      <div className={s.loginBlock}>
        <div>
          <NavLink
            to="/login"
            className={({ isActive, isPending }) =>
              isPending ? "pending" : isActive ? "active" : ""
            }
          >
            login
          </NavLink>
        </div>
      </div>
    </header>
  );
};

export default Header;
