import React from "react";
import s from "./App.module.css";

import Header from "./Components/Header/Header";
import Navbar from "./Components/Navbar/Navbar";
import Users from "./Components/Users/Users";
import Login from "./Components/Login/Login";
import MyPage from "./Components/MyPage/MyPage";

import { Route, Routes } from "react-router-dom";

function App() {
  return (
    <div className={s.appWrapper}>
      <Header />
      <Navbar />
      <div className={s.mainWrapper}>
        <Routes>
          <Route path="/" element={<MyPage />} />
          <Route path="/users" element={<Users />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
